[01]: img/01.png
[00]: img/00.png
[02]: img/02.png
[03]: img/03.png
[04]: img/04.png
[05]: img/05.png
[06]: img/06.png
[07]: img/07.png
[08]: img/08.png
[09]: img/09.png
[10]: img/10.png
[11]: img/11.png
[12]: img/12.png
[13]: img/13.png
[14]: img/14.png
[15]: img/15.png
[16]: img/16.png
[17]: img/17.png
[18]: img/18.png
[19]: img/19.png
[20]: img/20.png
[21]: img/21.png
[22]: img/22.png
[23]: img/23.png
[24]: img/24.png
[25]: img/25.png
[26]: img/26.png
[27]: img/27.png
[28]: img/28.png
[29]: img/29.png
[30]: img/30.png
[31]: img/31.png
[32]: img/32.png
[33]: img/33.png
[34]: img/34.png
[35]: img/35.png
[36]: img/36.png
[37]: img/37.png
[38]: img/38.png
[39]: img/39.png
[40]: img/40.png
[41]: img/41.png
[42]: img/42.png
[43]: img/43.png
[44]: img/44.png
[45]: img/45.png

# Reporte Compilación del Kernel de Linux
# Sistemas Operativos
### Marco Antonio Bravo García
### No. Cuenta 308037056

#### Preparación

Se realizó una instalación de Debian 8.9 en una maquina virtual.
![alt instalacion_debian][00]


Se realizó la instalación de los paquetes necesarios para la compilación

root@debian-marco-so:/mnt# aptitude install build-essential libncurses5-dev fakeroot

[  0%] Leyendo lista de paquetes
                                
[100%] Leyendo lista de paquetes
                                


[  0%] Creando árbol de dependencias
                                    
[100%] Creando árbol de dependencias
...

Una vez instalados se procedió a la descarga del kernel de linux en un archivo comprimido.

![alt descarga_comprimido][01]
![alt descarga_comprimido][02]
![alt descarga_comprimido][03]


Después descargamos la firma del kernel.

![alt descarga_comprimido][04]

Verificamos que los archivos que descargamos estén completos.

![alt descarga_comprimido][05]

Descomprimimos el archivo que contiene al kernel.

![alt descarga_comprimido][06]

Verificamos que la extracción haya sido exitosa.

![alt descarga_comprimido][07]

Verificamos la firma del kernel, la cual falla ya que no tenemos las llaves públicas de los desarrolladores

![alt descarga_comprimido][08]

Descargamos las llaves públicas para la firma del kernel y la verificamos.

![alt descarga_comprimido][10]

Descomprimimos el .tar del kernel para acceder a los archivos.
Copiamos la configuración del kernel actual para usarlo de base para el que vamos a compilar.

![alt descarga_comprimido][14]

Accedemos a la carpeta del kernel.

root@debian-marco-so:/mnt# cd linux-3.16.50

Y accedemos al menú de configuraciones del kernel.


![alt descarga_comprimido][16]

![alt descarga_comprimido][17]

Accedemos a General setup

![alt descarga_comprimido][18]

Cambiamos el nombre del kernel para distinguirlo, accedemos a Local version.

![alt descarga_comprimido][19]

Marcamos la opción de Automatically append version... y cambiamos el hostname a localhost

![alt descarga_comprimido][22]

Guardamos los cambios en el archivo .config y salimos.

![alt descarga_comprimido][23]

Ejecutamos make help para ver las opciones disponible para compilar.

![alt descarga_comprimido][24]
![alt descarga_comprimido][25]

Ejecutamos make all para comenzar con la compilación del kernel, la cual tardó algunas horas(3 y unos minutos).

![alt descarga_comprimido][26]
![alt descarga_comprimido][27]
![alt descarga_comprimido][28]
![alt descarga_comprimido][29]
![alt descarga_comprimido][30]
![alt descarga_comprimido][31]
![alt descarga_comprimido][32]

Verificamos que la compilación no haya tenido errores ejecutando echo $?, el cual regresa el codigo de error del último proceso ejecutado, si es 0, estamos seguros que todo salio bien.

![alt descarga_comprimido][33]

#### Después de esta primera parte continuaremos con crear los paquetes .deb para hacer el kernel que se acaba de compilar instalable.

Ejecutamos make deb-pkg para crear los paquetes .deb

![alt descarga_comprimido][37]
![alt descarga_comprimido][38]


Despues de que estan listos los paquetes, continuamos con su instalación. 

Ejecutamos dpkg -i  linux-firmware-image-3.16.50marco-bravo-308037056_3.16.50marco-bravo-308037056-2_amd64.deb 
linux-headers-3.16.50marco-bravo-308037056_3.16.50marco-bravo-308037056-2_amd64.deb 
linux-image-3.16.50marco-bravo-308037056-dbg_3.16.50marco-bravo-308037056-2_ammd64.deb 


![alt descarga_comprimido][43]

Cuando termina, verificamos que lo haya instalado.
Ejecutamos apt list --instaled '\*308037056'

![alt descarga_comprimido][44]

Para finalizar reiniciamos la maquina virtual y cuando carga el GRUB, elegimos opciones avanzadas y elegimos el kernel que acabamos de instalar para iniciar el sistema.

![alt descarga_comprimido][45]



# Dificultades 

Tuve varios problemas, como se puede notar en la imagen del final el nombre del kernel no coincide con el que se compilo, esto paso ya que primero había compilado con el nombre de "so-marcobravo-308037056", pero olvide tomar capturas de pantalla y el .log estaba incompleto, despues volvi a repetir todo pero el disco duro virtual era muy pequeño se lleno, no acabo de instalar la version con el nombre "marco-bravo-308037056", despues, al querer iniciar la maquina virtual, con la falta de espacio no cargaba el entorno grafico y me fue dificil sacar los .deb de la virtul a el anfitrión, borre la carpeta del kernel de linux, y ya entre a la interfaz pero fue demasiado tarde cuando note que de ahí necesitaba el .config, asi que tuve que realizar todo de nuevo a excepción de las capturas de pantalla.

Otro problema que tuve fue que al compilar en el nombre le puse guion bajo "\_" y al momento de crear los .deb me decia que no podia contener el caracter "\_" asi que tuve que hacer de nuevo todo

Realice 4 veces todo pero al final quedo bien.









